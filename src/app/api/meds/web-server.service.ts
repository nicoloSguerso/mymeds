import { Injectable } from '@angular/core';
import { HttpService } from '../web/http/http.service';
import { StorageService } from '../storage/storage.service';
import { ToastComponent } from 'src/app/component/toast/toast.component';
@Injectable({
  providedIn: 'root'
})
export class WebServerService {

  constructor(private http: HttpService, private storage: StorageService, 
              private toast: ToastComponent
            ) { }
  
  getProductByBarCodeLink:string="https://lam21.modron.network/products?barcode=";
  postProductPreferenceLink:string="https://lam21.modron.network/votes";
  postProductDetailLink: string="https://lam21.modron.network/products";
  sessionToken:string;
  accessToken:string;
  cd : string
  productList ;
  dt;

  public async getProductByBarCode(code:string){
   
    this.cd = code;
    let app_storage = await this.storage.get("app").then().catch(error =>{this.toast.presentToast("Error : storage: " +error.error, "middle" );});
    this.accessToken =  app_storage.access_token.token;
    let link = this.getProductByBarCodeLink+code;
    
    await this.http.get( link,
      {}, //param
      { //header
        Authorization: 'Bearer ' + this.accessToken
      } )
      .then(async (success)=>{
        this.dt = JSON.parse(success.data);
        this.productList = this.dt.products;
        this.sessionToken= this.dt.token;
       
        
      }, (error)=>{
        this.toast.presentToast("getProductByBarCode:Error:"+error.error, "middle");
       
      }).catch((error)=>{this.toast.presentToast("getProductByBarCode: catch:"+error,"bottom")})

    return  this.productList; 
  }


  param;
  public async postProductPreference(id :string){
   
    var link = this.postProductPreferenceLink;
    this.param = {
      "token": this.sessionToken,
      "rating": 1,
      "productId": id
      };
    var header = { Authorization: 'Bearer ' + this.accessToken}
      
    await this.http.post(link, this.param, header)
      .then(response=>{
        // this.toast.presentToast("PostProduct PREFERENCE : status : "+ response.status + "\nResponse data.id:" + response.data.id,"middle")
      })
      .catch(error=>{  this.toast.presentToast("PostProduct PREFERENCE : Catch "+ error.error,"middle")});
   
  }



}
