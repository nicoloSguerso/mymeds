import { Injectable } from '@angular/core';
import { HttpService } from '../web/http/http.service';
import { ToastComponent } from 'src/app/component/toast/toast.component';
import { WebServerService } from '../../api/meds/web-server.service';
import { StorageService } from '../storage/storage.service';


@Injectable({
  providedIn: 'root'
})
export class PostProductService {

  constructor(private httpService: HttpService, 
              private toast: ToastComponent, 
              private webserver: WebServerService, 
              private storage : StorageService
            ) { }


  async postPD(nome: string, descrizione: string, codice : string): Promise<any>{
    
    let link = this.webserver.postProductDetailLink;
    let sessionToken  = this.webserver.sessionToken;
    let app_storage = await this.storage.get("app").then().catch(error =>{this.toast.presentToast("Error : storage: " +error.error, "middle" );});
    let accessToken = app_storage.access_token.token;
    let param = {
        "token": sessionToken ,
        "name":  nome ,
        "description": descrizione,
        "barcode": codice ,
        "test": false
      };
    let header = { Authorization: 'Bearer ' + accessToken }
    

    let r = await this.httpService.post(link, param, header)
    .catch((error)=>{
      this.toast.presentToast("PostProductDetail : Catch/Status : "+ error.error, "bottom");
    });

    r.then((result)=>{
      // this.toast.presentToast("PostProductDetail : No Errors/Status : "+ result.status, "middle");
    })
    
    return r; 
  }
}
