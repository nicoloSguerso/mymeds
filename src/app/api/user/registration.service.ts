import { Injectable } from '@angular/core';
import { HttpService } from '../web/http/http.service';
import { StorageService } from '../storage/storage.service';
import { ToastComponent } from 'src/app/component/toast/toast.component';
import { WebServerService } from '../../api/meds/web-server.service';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  registrazioneLink: string = "https://lam21.modron.network/users"
  params: {}
  registrato : string ;
  constructor(private httpService: HttpService,
              private storageService: StorageService, 
              private toast : ToastComponent, 
              private webserver: WebServerService, 
            ) { }

  async registrazione(u: string, e: string, p:string):Promise<string>{
    this.params={
      "username": u,
      "email": e,
      "password": p
      };
      let user= "user_"+e;
      let app = "app_"+e;
    
    
    var promise = await this.httpService.post(this.registrazioneLink,this.params,{} )
      .then((response)=>{
        // this.toast.presentToast("Status Registrazione:" + response.status, "top");
        
        this.storageService.set(user , JSON.parse(response.data)).then(r=>console.log("user settato nello storage"));
        this.storageService.set(
          app ,{ "registrazione":true , "login":false,
            "access_token":{
              "token":"",
              "date":"",
              "date_expire":""
            }
          }).then(r=>console.log("app settato nello storage"));
        return this.registrato= response.status;
      },(error)=>{
        this.toast.presentToast("Status Registrazione:" + error.error, "top");
        return this.registrato= null;
      });
      this.toast.presentToast("Registrato :"+this.registrato, "middle");
      
      return  promise;
  }

}
