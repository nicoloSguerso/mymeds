import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../web/http/http.service';
import {ToastComponent} from '../../component/toast/toast.component';
import { StorageService } from '../storage/storage.service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  

  constructor(private router: Router,private http:HttpService, 
              private toast: ToastComponent, private storage: StorageService

            ) { }
  
  //variabili che questo servizio mette a disposizione
  loggato:boolean = false;
  username: string ="";
  password: string ="";
  email:string;
  loginLink:string = "https://lam21.modron.network/auth/login";
  params: {};
  // funzioni che questo servizio mette a disposizione
  prova(): void { console.log(" Dipendency Injection works!!!")}
  
  async login(e: string, p:string):Promise<string> { 
    //preparo la chiamata
    this.params={
      "email": e,
      "password": p
      }
    //eseguo la chiamata
    var promise :string = await this.http.post(this.loginLink,this.params )
      .then(async (response)=>{
        //info utili per la pagina tab1
        this.email = e; 
        this.password = p;
        this.loggato=true;
        // this.toast.presentToast("Status Login:" + response.status, "top");
        //memorizzo il token d'accesso da utilizzare per i prossimi giorni(7)
        var tk = JSON.parse(response.data).accessToken;
        //salvo il token d'accesso e aggiorno il file "app" nello storage
        let app = "app_"+e;
        let app_storage = await this.storage.get(app).then().catch(error =>{this.toast.presentToast("Error : " +error.error, "middle" )});
        
        app_storage.login = true;
        app_storage.access_token.token = tk;
        let d = new Date();
        let fd =new Date();
        fd.setDate(fd.getDate()+7);
        app_storage.access_token.date = d;
        app_storage.access_token.date_expire = fd;

        //aggiorno lo storage
        this.storage.set(app, app_storage).then(success =>{this.toast.presentToast("Success : nel aggiornamento di app storage ", "bottom")}).catch(error =>{this.toast.presentToast("Error : nel aggiornamento di app storage ", "bottom")});
      return "login eseguito con successo!";
      },(error)=>{
        this.toast.presentToast("Login Fallito: " + error.error, "bottom");
        return null;
      });
      
      
    return promise ;
  }
}


