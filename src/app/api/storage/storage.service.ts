import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastComponent } from '../../component/toast/toast.component';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private _storage: Storage | null = null;
  constructor(
              private storage: Storage,
              private toast : ToastComponent
              
) {  this.init()}

  async init(){
     const storage = await this.storage.create();
     this._storage = storage;
  }

  public set(key: string, value: any):any {
    return this._storage?.set(key, value);
  }
  
  public get(key: string):any {
    return this._storage?.get(key);
  }

  

  public async setMeds(value: any, meds_key):Promise<void>{
  
    let old:Object[] =[] ;
    old= await this.get(meds_key);
    let newS:Object[]= [] ;
    
    if(Boolean(old)){
      old.push(value);
      this.set(meds_key, old).then(r=> {
      }).catch(err=> this.toast.presentToast("Errore nel salvataggio della medicina nello storage","middle"));

    }else{
      newS.push(value);
      await this.set(meds_key, newS).then(r=> {
      }).catch(err=> this.toast.presentToast("Errore nel salvataggio della medicina nello storage","middle"));
  
    } 
    
  }

  public async getMeds(key:string){
   
  }

  public async deleteMeds(id, meds_key):Promise<boolean>{
    let index =0; 
    
    let storage = await this.storage.get(meds_key);
    let trovato = false;
    
    for(let item of storage){
      if(item.barcode == id){
        storage.splice(index, 1);
        trovato = true;
      }
      index ++;
    }
    
    if(trovato){
      await this.storage.set(meds_key, storage);
    }
    
    return trovato;
  }



  public async upDateMeds(key, value, meds_key):Promise<boolean>{
    let index =0; 
   
    let storage = await this.storage.get(meds_key);
    let trovato = false;
    
    for(let item of storage){
      if(item.barcode == key){
        storage.splice(index, 1, value);
        trovato = true;
      }
      index ++;
    }
    
    if(trovato){
       await this.storage.set(meds_key, storage);
    }
    
    return trovato;
    
   
  }

  //salva le medicine che vengono create
  public async trackMeds(value, trackMeds ){
   
    let old:Object[] =[] ;
    old= await this.get(trackMeds);
    let newS:Object[]= [] ;
    
    if(Boolean(old)){
      old.push(value);
      this.set(trackMeds, old).then(r=> {
      //  this.toast.presentToast("Medicina salvata nello storage(trackmeds)!","middle");
      }).catch(err=> this.toast.presentToast("Errore nel salvataggio della medicina nello storage","middle"));

    }else{
      // this.toast.presentToast("Storage vuoto (trackmeds)!","bottom");
      newS.push(value);
      await this.set(trackMeds, newS).then(r=> {
      }).catch(err=> this.toast.presentToast("Errore nel salvataggio della medicina nello storage","middle"));
  
    } 
    
  }

  public async getTrackMeds(trackMeds){
  
    return  this._storage?.get(trackMeds);

  }

  public async getTrackMedsByBarCode(trackMeds, barcode){
    // this.toast.presentToast("Metodo inizio", "bottom");
    let medRecenti = await this._storage?.get(trackMeds);
    let array:Object[]=[];
    
    for(let i of  medRecenti ){
      
      if(i.barcode == barcode){
        array.push(i);
        // await this.toast.presentToast("If : " + i.barcode, "top");
      }
    }
    // this.toast.presentToast("Metodo chiamato - array: + "+ JSON.stringify(array), "bottom");
    return array;
  }



}

  


