import { Injectable } from '@angular/core';
import { PopComponent } from '../../component/popover/pop/pop.component';
import { PopoverController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class PopoverService {
  manual:boolean = false;
  constructor(private PopComponent: PopComponent, 
              private popoverController: PopoverController
              
              ) { }

  manuale():void{
    this.manual = !this.manual;
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopComponent,
      cssClass: 'id#container{width:70vw;height:90vh;}',
      event: ev,
      translucent: true
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }


}
