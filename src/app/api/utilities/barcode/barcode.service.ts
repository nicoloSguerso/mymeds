import { Injectable } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ToastComponent } from 'src/app/component/toast/toast.component';
@Injectable({
  providedIn: 'root'
})
export class BarcodeService {
  data: any;
  constructor(private barcodeScanner: BarcodeScanner, private toast: ToastComponent) {}

  scan() {
    this.toast.presentToast("aperto scan()", "middle");
    this.data = null;
    this.barcodeScanner.scan().then(barcodeData => {
      this.data = barcodeData;
      return this.data;
    }).catch(err => {
      console.log('Error', err);
      return "error";
    });
  }
}
