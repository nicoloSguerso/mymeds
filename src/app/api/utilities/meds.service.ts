import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MedsService {
  meds:any[] = [];
  constructor() { }

  push(med:any){
    this.meds.push(med);  
    console.log("Medicine che sono state registrate : ");
    console.log(this.meds);
  }
  pull(){
    return this.meds;
  }
}
