import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ToastComponent } from 'src/app/component/toast/toast.component';
@Injectable({
  providedIn: 'root',
})
export class HttpService {
  
  constructor(private http:HTTP, private toast:ToastComponent) { }
  //http get request
  get(link:string, params?:any, headers?:any):any{  
    //ritorna una promessa su cui bisogna fare ovviamente .then(data=>{....})
    return this.http.get(link, params, headers);
  }
  post(link:string, params?:any, headers?:any):any{ 
    return this.http.post(link, params, headers);
  }
  
}

