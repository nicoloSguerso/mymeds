import { Component , OnInit} from '@angular/core';
import { LoginService } from '../api/user/login.service';
import { StorageService } from '../api/storage/storage.service';
import { ToastComponent } from '../component/toast/toast.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  Titolo: string
  loggato:boolean
  username: string
  password: string
  email:string
  utente;
 
  constructor(  private loginService: LoginService,
                private storage:StorageService, private toast : ToastComponent
                
              ) {}
  async ngOnInit() {
    this.Titolo = "Area personale"
    this.loggato= this.loginService.loggato
    console.log("Tab1 : ngOnInit : "+this.loggato)
    let user = "user_"+this.loginService.email;
    await this.storage.get(user).then(data =>{ this.utente = data;}).catch(err=>{this.toast.presentToast("error:"+ err, "bottom")});
    this.username= this.utente.username; 
    this.email = this.utente.email; 
    this.password= this.utente.password;
  }
  u : string; 
  e:string;
  p: string;
  async ionViewWillEnter(){
    this.loggato= this.loginService.loggato
    console.log("Tab1 : ionViewWillEnter : "+ this.loggato)
    
    let user = "user_"+this.loginService.email;
    await this.storage.get(user).then(data =>{ this.utente = data;}).catch(err=>{this.toast.presentToast("error:"+ err, "bottom")});    this.username= this.utente.username; 
    this.email = this.utente.email; 
    this.password= this.utente.password;
  }

 


 
  

}
