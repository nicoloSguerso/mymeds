import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { MedsService} from '../api/utilities/meds.service'
import { Router } from '@angular/router';
import { WebServerService } from '../api/meds/web-server.service';
import { ToastComponent } from '../component/toast/toast.component';
import { IonInfiniteScroll } from '@ionic/angular';
import { HttpService } from '../api/web/http/http.service';
import { PostProductService } from '../api/meds/post-product.service';
import { RegistrationService } from '../api/user/registration.service';
import { StorageService } from '../api/storage/storage.service';
import { LoginService } from '../api/user/login.service'
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  Titolo: string
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor( 
              private meds: MedsService, 
              private router: Router, 
              
              private webserver:WebServerService, 
              private toast : ToastComponent, 
              private http : HttpService, 
              private postProductService : PostProductService, 
              private registrationsService : RegistrationService, 
              private storageService : StorageService,
              private loginService : LoginService, 

              private barcodeScanner: BarcodeScanner 
             
            ) {}

  ngOnInit() {
    this.Titolo="Aggiungi Farmaco";
    if(!this.loginService.loggato)
      this.router.navigate(["login"]);
    
  }

  ionViewWillEnter() {
    
    if(!this.loginService.loggato)
      this.router.navigate(["login"]);
  }



  inserisci:boolean = true;// true se è selezionato la modalita scan!
  segmentChanged(e: any) { 
    if(this.inserisci)
     this.inserisci = false;
    else this.inserisci = true;
    console.log("tab3 : segmentChange: ", e.detail.value , " --- scan: " , this.inserisci);
  }

  id:string
  productList;
  trackMeds;
  async onCerca(): Promise<void>{
    let x:string = this.id;
    if(x.replace(" ", "")){
     
      this.productList = await this.webserver.getProductByBarCode(x);
      let trackID = "trackMeds_"+ this.loginService.email;
      this.trackMeds = await this.storageService.getTrackMedsByBarCode(trackID, x);      
    }
    
  }
  
 
  public onAggiungi(item){
    let data_default = new Date("2000-01-01");
    let key_meds = "Meds_"+this.loginService.email;

    this.storageService.setMeds(
      {
        "barcode": item.barcode, 
        "name": item.name,
        "description": item.description,
        "date_expire": data_default
      }, 
      key_meds
    ).then(r=> {
      this.meds.push(this.id);  
      this.id="";
      this.productList =null;
      this.router.navigate(["/tabs/tab2"]);
    });
   
    
   
  }

  public async onRate(item):Promise<void>{
    await this.webserver.postProductPreference(item.id);
  }


  nomeMedicina:string;
  descrizione: string;
  dataScadenza;
  risposta;


  public async onSalva(){
    let barcode = this.id;
    
    let dataS = new Date(this.dataScadenza);
    let key_meds = "Meds_"+this.loginService.email;
    let nuovaMedicina =  {
      "barcode": barcode ,
      "name":  this.nomeMedicina ,
      "description": this.descrizione,
      "date_expire": dataS
    };
    let trackMeds = "trackMeds_"+this.loginService.email;
    this.storageService.setMeds( nuovaMedicina, key_meds ).then(r=> {

      this.storageService.trackMeds(nuovaMedicina, trackMeds);

      this.meds.push(barcode);
      this.id="";
      this.nomeMedicina="";
      this.descrizione="";
      this.dataScadenza="";
      this.productList =null;
      this.router.navigate(["/tabs/tab2"]);
    });

    
    this.risposta = await this.postProductService.postPD(this.nomeMedicina, this.descrizione , barcode);
    this.risposta = JSON.stringify(this.risposta);
    this.router.navigate(["/tabs/tab2"]);
  }



  scannedData: any;
  encodedData: '';
  encodeData: any;
  
  scanBarcode() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      formats: 'EAN_13,EAN_8,QR_CODE,PDF_417 ',
      orientation: 'portrait',
    };

    this.barcodeScanner.scan(options).then(barcodeData => {
      this.scannedData = barcodeData;

    }).catch(err => {
      console.log('Error', err);
    });
  }




  
  trackItems(index: number, itemObject: any) {
    return itemObject.id;
  }
  trackTrackedItem(index: number, itemObject: any) {
    return itemObject.id;
  }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.productList.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
  


 

}
