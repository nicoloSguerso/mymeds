import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/api/user/registration.service';
import { Router } from '@angular/router';
import { ToastComponent } from 'src/app/component/toast/toast.component';
@Component({
  selector: 'app-registrazione',
  templateUrl: './registrazione.page.html',
  styleUrls: ['./registrazione.page.scss'],
})
export class RegistrazionePage implements OnInit {

  email: string;
  password: string;
  username: string;
  constructor(private registration:RegistrationService, private router: Router, 
              private toast : ToastComponent 
            ) { }

  ngOnInit() {
    this.email="";
    this.password="";
    this.username="";
  }

  async onRegistrazione(){
   var status :string;
   await this.registration.registrazione(this.username,this.email, this.password)
    .then(text => {status = text;});
   this.username ="";
   this.email="";
   this.password="";
   if(status ){
    console.log("registrazione avvenuta con successo")
    //faccio partire un pop over per dire che la registrazione è avvenuta con successo
    // e poi faccio un router per spostarmi nella tabella utente
    this.router.navigate(['/tabs/tab1']);
    this.toast.presentToast("Registrazione avvenuta con successo!", "bottom");
   }else{
     //faccio uscire un toast per dire che che la registrazione è stata negata, caso di omonimi
     console.log("registrazione fallita") 
     this.toast.presentToast("Registrazione negata, si prega di cambiare dati", "bottom");
    }
    
  }

}
