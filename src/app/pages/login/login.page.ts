import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoginService } from 'src/app/api/user/login.service';
import { Router } from '@angular/router';
import { ToastComponent } from 'src/app/component/toast/toast.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private loginService: LoginService, private router:Router,
              private toast: ToastComponent
            ) { }
  
  
  email:string
  password:string
  
  ngOnInit() {
    this.email=""
    this.password= ""
    console.log("Login : ngOnInit : " + this.loginService.loggato )

  }
  
  
  async onLogin(): Promise<void>{
    var r = await this.loginService.login(this.email, this.password);
    this.email ="";
    this.password="";
    if(r){
      this.router.navigate(["/tabs/tab2"]); 
    }
         
  }

}
