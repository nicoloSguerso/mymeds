import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})
export class ToastComponent implements OnInit {

  constructor(public toastController: ToastController) {}
  ngOnInit() {}

  async presentToast(text : string, pos:"top"|"bottom"|"middle") {
    const toast = await this.toastController.create({
      message: text,
      position: pos, 
      duration:10000
    });
    toast.present();
  }

}
