import { Component, OnInit, OnChanges } from '@angular/core';
import { Injectable } from '@angular/core';
import { WebServerService } from '../../../api/meds/web-server.service';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-pop',
  templateUrl: './pop.component.html',
  styleUrls: ['./pop.component.scss'],
})
export class PopComponent implements OnInit {
  manual:boolean
  list:{};
  mostraLista:boolean;
  constructor( private webserver: WebServerService) { }

  ngOnInit() {
    this.manual=false;
    this.list = this.webserver.productList;
    if(this.list){
      this.mostraLista=true;
    }else{this.mostraLista=false;}
  }


  ionViewWillEnter(){
    this.list = this.webserver.productList;
    if(this.list){
      this.mostraLista=true;
    }else{this.mostraLista=false;}
  }

  trackItems(index: number, itemObject: any) {
    return itemObject.id;
  }


  

}
