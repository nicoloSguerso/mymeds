import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ToastComponent } from '../toast/toast.component';
import { StorageService } from 'src/app/api/storage/storage.service';
import { LoginService } from '../../api/user/login.service'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {

  constructor(  
                public modalController: ModalController, 
                private toast: ToastComponent,
                private storageService: StorageService, 
                private loginService : LoginService
                
              ) { }

  salvaModifiche=false;
  dateM=false;
  descriptionM = false;
  dataScadenza:Date;
  ngOnInit() {}

  @Input() item: any;

  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  onCambioDate(newDate){
    this.item.date_expire = new Date(newDate);
    this.salvaModifiche=true;
  }


  onCambioDescrizione(newDescription){
    this.item.description = newDescription;
    this.salvaModifiche=true;
  }

  onSalvaModifiche(){
    if(this.salvaModifiche){
      let meds_key = "Meds_"+this.loginService.email;
      if(this.storageService.upDateMeds(this.item.barcode, this.item, meds_key)){
        this.dismissModal();
      }
       

    }
  }

 

}
