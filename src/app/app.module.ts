import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP } from '@ionic-native/http/ngx';
import { Network } from '@ionic-native/network/ngx'
import { IonicStorageModule } from '@ionic/storage-angular';
import { Drivers } from '@ionic/storage';
import { ToastComponent } from './component/toast/toast.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { PopModule } from './component/popover/pop/pop.module';
import { PopComponent } from './component/popover/pop/pop.component'
import { HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, IonicModule.forRoot(), 
    AppRoutingModule, 
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
    }), 
    PopModule
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    HTTP,
    Network,
    ToastComponent, 
    BarcodeScanner, 
    PopModule, 
    PopComponent, 
    HttpClient, 
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
