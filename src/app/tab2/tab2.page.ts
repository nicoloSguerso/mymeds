import { Component, OnInit, OnChanges } from '@angular/core';
import {MedsService} from '../api/utilities/meds.service'
import { StorageService } from '../api/storage/storage.service';
import { ToastComponent } from '../component/toast/toast.component';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../component/modal/modal.component';
import { LoginService } from '../api/user/login.service'


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  Titolo: string
  items: any
  searchMed:string;
  oggi= new Date();
  trackM;

  constructor(private meds: MedsService, private storage : StorageService, 
              private toast: ToastComponent, 
              private router: Router, 
              public modalController: ModalController, 
              private loginService : LoginService
             ){}


  ngOnInit() {
    this.Titolo ="Le tue medicine"
    if(!this.loginService.loggato){
      this.router.navigate(["login"]);
    }
  
    this.reloadData();
  } 

  app_s:string;
  access_token: string;
  medicine:any;
  m:string;
  login:boolean;
  async ionViewWillEnter(){

    if(this.loginService.loggato){
    //this.items = this.meds.pull();
    //console.log("tabs 2 : items : \n",this.items);
    
    //temporaneo
    let app;
    let key= "app_"+this.loginService.email;
    await this.storage.get(key).then(d=>{ app = d; }).catch(d=>{this.toast.presentToast("tab2 : Error"+d,"middle")});
    this.app_s = JSON.stringify(app);
    this.access_token = app.access_token.token;
    this.login = app.login;
   
    await this.reloadData()
    

    }else{
      this.router.navigate(["login"]);
    }

  }

  trackMeds(index: number, itemObject: any) {
    return itemObject.id;
  }

   async doRefresh(event, millis){
    setTimeout(async () => {
      await this.reloadData();
      event.target.complete();
    }, millis);
  }

  async onElimina(id, event){
    let key_meds = "Meds_"+this.loginService.email;
    if(this.storage.deleteMeds(id, key_meds)){
      this.doRefresh(event, 300);

    } 
  }


  async reloadData(){
    let key_meds = "Meds_"+this.loginService.email;
    let x= await this.storage.get(key_meds);
    this.medicine =x;
    this.m = JSON.stringify(x);
    
    let trackMeds = "trackMeds_"+this.loginService.email;
    this.trackM  = await this.storage.getTrackMeds(trackMeds);
    this.m = JSON.stringify(this.trackM);
   
  
}
  


async presentModal(med:any) {
  const modal = await this.modalController.create({
    component: ModalComponent,
    cssClass: 'my-custom-class',
    componentProps: {item: med}
  });
  return await modal.present();
}

  scaduto(date):any{
    let oggi = new Date();
    let bool= false;
    if(date < oggi ){
      bool = true; 
    }
   return bool;
  }

  nullo(date){
    let oggi = new Date();
    
    if(date == null){
      return true
    }else{
      return false
    }
  }

  valido(date){
    let oggi = new Date();
    if(date > oggi){
      return true
    }else{
      return false
    }

  }
  
  
}
function moment(date: any) {
  throw new Error('Function not implemented.');
}

